import { isValidEmail } from "../functions/validate"
import { _alert, _confirm } from "../functions/message";
import { setInStorage, checkStorage, getFromStorage } from "../functions/storage";


$(document).ready(function () {
    $("#valorNome_sessao").text(checkStorage("session_valorNome") ? getFromStorage("session_valorNome") : "");
    $("#valorEmail_sessao").text(checkStorage("session_valorEmail") ? getFromStorage("session_valorEmail") : "");
    $("#valorTelefone_sessao").text(checkStorage("session_valorTelefone") ? getFromStorage("session_valorTelefone") : "");
    $("#valorAssunto_sessao").text(checkStorage("session_valorAssunto") ? getFromStorage("session_valorAssunto") : "");
    $("#valorMensagem_sessao").text(checkStorage("session_valorMensagem") ? getFromStorage("session_valorMensagem") : "");
});


$(document).on("keyup", "#formFrontEnd input[name*='Nome']", function (e) {
    $("#valorNome").text($(this).val());
});

$(document).on("keyup", "#formFrontEnd input[name*='Email']", function (e) {
    $("#valorEmail").text($(this).val());
});

$(document).on("keyup", "#formFrontEnd input[name*='Telefone']", function (e) {
    $("#valorTelefone").text($(this).val());
});

$(document).on("keyup", "#formFrontEnd input[name*='Assunto']", function (e) {
    $("#valorAssunto").text($(this).val());
});

$(document).on("keyup", "#formFrontEnd textarea[name*='Mensagem']", function (e) {
    $("#valorMensagem").text($(this).val());
});


$(document).on("click", "#submitHotSite", function (e) {

    var count = 0,
        form = $("#formFrontEnd");

    $("textarea:not(.g-recaptcha-response), input:visible:not([type='checkbox']):not(.search)", form).each(function () {
        if ($(this).val().trim() === "")
            count++;

        if ($(this).val().trim() === "")
            console.log($(this))
    });

    if (count === 0) {

        var _email = $("#formFrontEnd input[name*='Email']").val();
        if (!isValidEmail(_email)) {
            _alert("", "Email inválido.", "warning");
            return false;
        }

        var _telefone = $("#formFrontEnd input[name*='Telefone']").val();
        if (!isValidPhone(_telefone)) {
            _alert("", "Telefone inválido.", "warning");
            return false;
        }

        sendFrontEnd();

    } else {
        _alert("Atenção!", "Faça as correções necessárias nos campos obrigatórios.", "error");
    }

    return false;
})


function isValidPhone(phoneToValidate) {

    var phone = new RegExp(/(^|\()?\s*(\d{2})\s*(\s|\))*(\s|-)?(\d{4})($|\n)/u);

    if (phone.test(phoneToValidate)) {
        _alert("", "Telefone inválido.", "warning");
        return true
    }
    else
        return false
}

function sendFrontEnd() {

    var form = $("#formFrontEnd").serializeArray();
    var form_data = {};
    $.each(form, function (i, v) {
        form_data[v.name] = v.value;
    });

    setInStorage("session_valorNome", form_data["Nome"]);
    setInStorage("session_valorEmail", form_data["Email"]);
    setInStorage("session_valorTelefone", form_data["Telefone"]);
    setInStorage("session_valorAssunto", form_data["Assunto"]);
    setInStorage("session_valorMensagem", form_data["Mensagem"]);

    _confirm({
        title: "",
        text: "Dados enviados com sucesso.",
        type: "success",
        confirm: {
            text: "OK"
        },
        cancel: {
            text: "Cancelar"
        },
        callback: function () {
            location.reload();
        }
    }, false);

}
